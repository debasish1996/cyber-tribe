const mongoose = require('mongoose');
const uri = process.env.MONGODB_URI
exports.connect = (callback) => {
  return mongoose
    .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(callback)
    .catch((err) => {
      console.log(err);
    });
};